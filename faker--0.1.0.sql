
-- initialize the faker engine
-- usage: SELECT faker.faker(ARRAY('fr_FR','ja_JP'))
CREATE OR REPLACE FUNCTION faker(
  locales TEXT[]
)
RETURNS BOOLEAN
AS $$
  from faker import Faker
  GD['Faker'] = Faker(locales)
  return True
$$
  LANGUAGE plpython3u
;

--
-- initialize the faker engine with one or zero locale
-- usage:
--  - SELECT faker.faker()
--  - SELECT faker.faker('de_DE')
--
CREATE OR REPLACE FUNCTION faker(
  locale TEXT DEFAULT 'en_US'
)
RETURNS BOOLEAN
AS $$
  from faker import Faker
  GD['Faker'] = Faker(locale)
  return True
$$
  LANGUAGE plpython3u
;

--
-- Seeding the generator
-- https://github.com/joke2k/faker#seeding-the-generator
--
CREATE OR REPLACE FUNCTION seed(
  seed TEXT
)
RETURNS BOOLEAN
AS $$
  try:
    GD['Faker'].seed_instance(seed)
  except KeyError:
    plpy.error("faker is not initialized.",
                hint="Use SELECT faker.faker(); first.")
  return True
$$
  LANGUAGE plpython3u
;

CREATE OR REPLACE FUNCTION seed(
  seed INTEGER
)
RETURNS BOOLEAN
AS $$
  try:
    GD['Faker'].seed_instance(seed)
  except KeyError:
    plpy.error("faker is not initialized.",
                hint="Use SELECT faker.faker(); first.")
  return True
$$
  LANGUAGE plpython3u
;



-- Provider : address
-- https://github.com/joke2k/faker/blob/master/faker/providers/address/__init__.py


CREATE OR REPLACE FUNCTION city_suffix() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].city_suffix()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION street_suffix() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].street_suffix()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION building_number() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].building_number()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION city() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].city()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION street_name() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].street_name()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION street_address() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].street_address()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION postcode() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].postcode()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION address() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].address()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION country() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].country()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION country_code("representation" TEXT = 'alpha-2' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].country_code(representation
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : automotive
-- https://github.com/joke2k/faker/blob/master/faker/providers/automotive/__init__.py


CREATE OR REPLACE FUNCTION license_plate() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].license_plate()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : bank
-- https://github.com/joke2k/faker/blob/master/faker/providers/bank/__init__.py


CREATE OR REPLACE FUNCTION bank_country() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].bank_country()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION bban() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].bban()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION iban() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].iban()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : barcode
-- https://github.com/joke2k/faker/blob/master/faker/providers/barcode/__init__.py


CREATE OR REPLACE FUNCTION ean("length" INTEGER = 13 ,"prefixes" TEXT[] = ARRAY[]::TEXT[] ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].ean(length
,prefixes
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION ean8("prefixes" TEXT[] = ARRAY[]::TEXT[] ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].ean8(prefixes
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION ean13("prefixes" TEXT[] = ARRAY[]::TEXT[] ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].ean13(prefixes
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION localized_ean("length" INTEGER = 13 ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].localized_ean(length
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION localized_ean8() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].localized_ean8()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION localized_ean13() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].localized_ean13()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : color
-- https://github.com/joke2k/faker/blob/master/faker/providers/color/__init__.py


CREATE OR REPLACE FUNCTION color_name() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].color_name()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION safe_color_name() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].safe_color_name()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION hex_color() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].hex_color()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION safe_hex_color() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].safe_hex_color()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION rgb_color() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].rgb_color()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION rgb_css_color() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].rgb_css_color()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION color("hue" TEXT = 'None' ,"luminosity" TEXT = 'None' ,"color_format" TEXT = 'hex' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].color(hue
,luminosity
,color_format
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : company
-- https://github.com/joke2k/faker/blob/master/faker/providers/company/__init__.py


CREATE OR REPLACE FUNCTION company() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].company()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION company_suffix() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].company_suffix()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION catch_phrase() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].catch_phrase()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION bs() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].bs()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : credit_card
-- https://github.com/joke2k/faker/blob/master/faker/providers/credit_card/__init__.py


CREATE OR REPLACE FUNCTION credit_card_provider("card_type" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].credit_card_provider(card_type
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION credit_card_number("card_type" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].credit_card_number(card_type
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION credit_card_expire("start" TEXT = 'now' ,"end" TEXT = '+10y' ,"date_format" TEXT = '%m/%y' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].credit_card_expire(start
,end
,date_format
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION credit_card_full("card_type" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].credit_card_full(card_type
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION credit_card_security_code("card_type" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].credit_card_security_code(card_type
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : currency
-- https://github.com/joke2k/faker/blob/master/faker/providers/currency/__init__.py


CREATE OR REPLACE FUNCTION currency() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].currency()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION currency_code() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].currency_code()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION currency_name() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].currency_name()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION currency_symbol("code" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].currency_symbol(code
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION cryptocurrency() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].cryptocurrency()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION cryptocurrency_code() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].cryptocurrency_code()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION cryptocurrency_name() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].cryptocurrency_name()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : date_time
-- https://github.com/joke2k/faker/blob/master/faker/providers/date_time/__init__.py


CREATE OR REPLACE FUNCTION unix_time("end_datetime" TEXT = 'None' ,"start_datetime" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].unix_time(end_datetime
,start_datetime
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION time_delta("end_datetime" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].time_delta(end_datetime
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_time("tzinfo" TEXT = 'None' ,"end_datetime" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_time(tzinfo
,end_datetime
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_time_ad("tzinfo" TEXT = 'None' ,"end_datetime" TEXT = 'None' ,"start_datetime" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_time_ad(tzinfo
,end_datetime
,start_datetime
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION iso8601("tzinfo" TEXT = 'None' ,"end_datetime" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].iso8601(tzinfo
,end_datetime
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date("pattern" TEXT = '%Y-%m-%d' ,"end_datetime" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date(pattern
,end_datetime
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_object("end_datetime" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_object(end_datetime
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION time_object("end_datetime" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].time_object(end_datetime
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_time_between("start_date" TEXT = '-30y' ,"end_date" TEXT = 'now' ,"tzinfo" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_time_between(start_date
,end_date
,tzinfo
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_between("start_date" TEXT = '-30y' ,"end_date" TEXT = 'today' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_between(start_date
,end_date
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION future_datetime("end_date" TEXT = '+30d' ,"tzinfo" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].future_datetime(end_date
,tzinfo
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION future_date("end_date" TEXT = '+30d' ,"tzinfo" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].future_date(end_date
,tzinfo
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION past_datetime("start_date" TEXT = '-30d' ,"tzinfo" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].past_datetime(start_date
,tzinfo
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION past_date("start_date" TEXT = '-30d' ,"tzinfo" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].past_date(start_date
,tzinfo
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_time_between_dates("datetime_start" TEXT = 'None' ,"datetime_end" TEXT = 'None' ,"tzinfo" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_time_between_dates(datetime_start
,datetime_end
,tzinfo
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_between_dates("date_start" TEXT = 'None' ,"date_end" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_between_dates(date_start
,date_end
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_time_this_century("before_now" BOOLEAN = True ,"after_now" BOOLEAN = False ,"tzinfo" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_time_this_century(before_now
,after_now
,tzinfo
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_time_this_decade("before_now" BOOLEAN = True ,"after_now" BOOLEAN = False ,"tzinfo" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_time_this_decade(before_now
,after_now
,tzinfo
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_time_this_year("before_now" BOOLEAN = True ,"after_now" BOOLEAN = False ,"tzinfo" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_time_this_year(before_now
,after_now
,tzinfo
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_time_this_month("before_now" BOOLEAN = True ,"after_now" BOOLEAN = False ,"tzinfo" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_time_this_month(before_now
,after_now
,tzinfo
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_this_century("before_today" BOOLEAN = True ,"after_today" BOOLEAN = False ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_this_century(before_today
,after_today
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_this_decade("before_today" BOOLEAN = True ,"after_today" BOOLEAN = False ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_this_decade(before_today
,after_today
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_this_year("before_today" BOOLEAN = True ,"after_today" BOOLEAN = False ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_this_year(before_today
,after_today
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_this_month("before_today" BOOLEAN = True ,"after_today" BOOLEAN = False ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_this_month(before_today
,after_today
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION time_series("start_date" TEXT = '-30d' ,"end_date" TEXT = 'now' ,"precision" TEXT = 'None' ,"distrib" TEXT = 'None' ,"tzinfo" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].time_series(start_date
,end_date
,precision
,distrib
,tzinfo
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION am_pm() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].am_pm()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION day_of_month() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].day_of_month()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION day_of_week() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].day_of_week()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION month() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].month()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION month_name() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].month_name()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION year() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].year()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION century() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].century()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION timezone() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].timezone()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION pytimezone("args" TEXT = '' ,"kwargs" TEXT = '' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].pytimezone(args
,kwargs
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION date_of_birth("tzinfo" TEXT = 'None' ,"minimum_age" INTEGER = 0 ,"maximum_age" INTEGER = 115 ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].date_of_birth(tzinfo
,minimum_age
,maximum_age
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : file
-- https://github.com/joke2k/faker/blob/master/faker/providers/file/__init__.py


CREATE OR REPLACE FUNCTION mime_type("category" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].mime_type(category
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION file_name("category" TEXT = 'None' ,"extension" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].file_name(category
,extension
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION file_extension("category" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].file_extension(category
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION file_path("depth" INTEGER = 1 ,"category" TEXT = 'None' ,"extension" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].file_path(depth
,category
,extension
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION unix_device("prefix" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].unix_device(prefix
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION unix_partition("prefix" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].unix_partition(prefix
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : geo
-- https://github.com/joke2k/faker/blob/master/faker/providers/geo/__init__.py


CREATE OR REPLACE FUNCTION coordinate("center" TEXT = 'None' ,"radius" FLOAT = 0.001 ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].coordinate(center
,radius
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION latitude() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].latitude()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION longitude() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].longitude()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION latlng() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].latlng()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION local_latlng("country_code" TEXT = 'US' ,"coords_only" BOOLEAN = False ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].local_latlng(country_code
,coords_only
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION location_on_land("coords_only" BOOLEAN = False ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].location_on_land(coords_only
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : internet
-- https://github.com/joke2k/faker/blob/master/faker/providers/internet/__init__.py


CREATE OR REPLACE FUNCTION email("domain" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].email(domain
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION safe_email() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].safe_email()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION free_email() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].free_email()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION company_email() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].company_email()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION free_email_domain() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].free_email_domain()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION ascii_email() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].ascii_email()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION ascii_safe_email() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].ascii_safe_email()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION ascii_free_email() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].ascii_free_email()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION ascii_company_email() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].ascii_company_email()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION user_name() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].user_name()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION hostname("levels" INTEGER = 1 ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].hostname(levels
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION domain_name("levels" INTEGER = 1 ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].domain_name(levels
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION domain_word() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].domain_word()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION dga("year" TEXT = 'None' ,"month" TEXT = 'None' ,"day" TEXT = 'None' ,"tld" TEXT = 'None' ,"length" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].dga(year
,month
,day
,tld
,length
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION tld() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].tld()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION http_method() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].http_method()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION url("schemes" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].url(schemes
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION ipv4_network_class() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].ipv4_network_class()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION ipv4("network" BOOLEAN = False ,"address_class" TEXT = 'None' ,"private" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].ipv4(network
,address_class
,private
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION ipv4_private("network" BOOLEAN = False ,"address_class" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].ipv4_private(network
,address_class
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION ipv4_public("network" BOOLEAN = False ,"address_class" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].ipv4_public(network
,address_class
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION ipv6("network" BOOLEAN = False ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].ipv6(network
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION mac_address() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].mac_address()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION port_number("is_system" BOOLEAN = False ,"is_user" BOOLEAN = False ,"is_dynamic" BOOLEAN = False ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].port_number(is_system
,is_user
,is_dynamic
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION uri_page() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].uri_page()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION uri_path("deep" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].uri_path(deep
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION uri_extension() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].uri_extension()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION uri() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].uri()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION slug("value" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].slug(value
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION image_url("width" TEXT = 'None' ,"height" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].image_url(width
,height
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : isbn
-- https://github.com/joke2k/faker/blob/master/faker/providers/isbn/__init__.py


CREATE OR REPLACE FUNCTION isbn13("separator" TEXT = '-' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].isbn13(separator
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION isbn10("separator" TEXT = '-' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].isbn10(separator
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : job
-- https://github.com/joke2k/faker/blob/master/faker/providers/job/__init__.py


CREATE OR REPLACE FUNCTION job() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].job()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : lorem
-- https://github.com/joke2k/faker/blob/master/faker/providers/lorem/__init__.py


CREATE OR REPLACE FUNCTION words("nb" INTEGER = 3 ,"ext_word_list" TEXT = 'None' ,"unique" BOOLEAN = False ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].words(nb
,ext_word_list
,unique
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION word("ext_word_list" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].word(ext_word_list
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION sentence("nb_words" INTEGER = 6 ,"variable_nb_words" BOOLEAN = True ,"ext_word_list" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].sentence(nb_words
,variable_nb_words
,ext_word_list
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION sentences("nb" INTEGER = 3 ,"ext_word_list" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].sentences(nb
,ext_word_list
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION paragraph("nb_sentences" INTEGER = 3 ,"variable_nb_sentences" BOOLEAN = True ,"ext_word_list" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].paragraph(nb_sentences
,variable_nb_sentences
,ext_word_list
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION paragraphs("nb" INTEGER = 3 ,"ext_word_list" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].paragraphs(nb
,ext_word_list
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION text("max_nb_chars" INTEGER = 200 ,"ext_word_list" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].text(max_nb_chars
,ext_word_list
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION texts("nb_texts" INTEGER = 3 ,"max_nb_chars" INTEGER = 200 ,"ext_word_list" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].texts(nb_texts
,max_nb_chars
,ext_word_list
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : misc
-- https://github.com/joke2k/faker/blob/master/faker/providers/misc/__init__.py


CREATE OR REPLACE FUNCTION null_boolean() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].null_boolean()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION binary("length" INTEGER = 1048576 ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].binary(length
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION md5("raw_output" BOOLEAN = False ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].md5(raw_output
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION sha1("raw_output" BOOLEAN = False ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].sha1(raw_output
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION sha256("raw_output" BOOLEAN = False ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].sha256(raw_output
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION password("length" INTEGER = 10 ,"special_chars" BOOLEAN = True ,"digits" BOOLEAN = True ,"upper_case" BOOLEAN = True ,"lower_case" BOOLEAN = True ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].password(length
,special_chars
,digits
,upper_case
,lower_case
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION zip("uncompressed_size" INTEGER = 65536 ,"num_files" INTEGER = 1 ,"min_file_size" INTEGER = 4096 ,"compression" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].zip(uncompressed_size
,num_files
,min_file_size
,compression
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION tar("uncompressed_size" INTEGER = 65536 ,"num_files" INTEGER = 1 ,"min_file_size" INTEGER = 4096 ,"compression" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].tar(uncompressed_size
,num_files
,min_file_size
,compression
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : person
-- https://github.com/joke2k/faker/blob/master/faker/providers/person/__init__.py


CREATE OR REPLACE FUNCTION name() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].name()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION first_name() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].first_name()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION last_name() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].last_name()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION name_male() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].name_male()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION name_female() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].name_female()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION first_name_male() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].first_name_male()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION first_name_female() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].first_name_female()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION last_name_male() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].last_name_male()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION last_name_female() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].last_name_female()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION prefix() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].prefix()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION prefix_male() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].prefix_male()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION prefix_female() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].prefix_female()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION suffix() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].suffix()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION suffix_male() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].suffix_male()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION suffix_female() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].suffix_female()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION language_name() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].language_name()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : phone_number
-- https://github.com/joke2k/faker/blob/master/faker/providers/phone_number/__init__.py


CREATE OR REPLACE FUNCTION phone_number() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].phone_number()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION country_calling_code() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].country_calling_code()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION msisdn() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].msisdn()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : profile
-- https://github.com/joke2k/faker/blob/master/faker/providers/profile/__init__.py


CREATE OR REPLACE FUNCTION simple_profile("sex" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].simple_profile(sex
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION profile("fields" TEXT = 'None' ,"sex" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].profile(fields
,sex
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : python
-- https://github.com/joke2k/faker/blob/master/faker/providers/python/__init__.py


CREATE OR REPLACE FUNCTION pybool() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].pybool()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION pystr("min_chars" TEXT = 'None' ,"max_chars" INTEGER = 20 ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].pystr(min_chars
,max_chars
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION pystr_format("string_format" TEXT = '?#-###{{random_int}}{{random_letter}}' ,"letters" TEXT = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].pystr_format(string_format
,letters
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION pyfloat("left_digits" TEXT = 'None' ,"right_digits" TEXT = 'None' ,"positive" BOOLEAN = False ,"min_value" TEXT = 'None' ,"max_value" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].pyfloat(left_digits
,right_digits
,positive
,min_value
,max_value
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION pyint("min_value" INTEGER = 0 ,"max_value" INTEGER = 9999 ,"step" INTEGER = 1 ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].pyint(min_value
,max_value
,step
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION pydecimal("left_digits" TEXT = 'None' ,"right_digits" TEXT = 'None' ,"positive" BOOLEAN = False ,"min_value" TEXT = 'None' ,"max_value" TEXT = 'None' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].pydecimal(left_digits
,right_digits
,positive
,min_value
,max_value
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION pytuple("nb_elements" INTEGER = 10 ,"variable_nb_elements" BOOLEAN = True ,"value_types" TEXT = 'None' ,"allowed_types" TEXT = '' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].pytuple(nb_elements
,variable_nb_elements
,value_types
,allowed_types
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION pyset("nb_elements" INTEGER = 10 ,"variable_nb_elements" BOOLEAN = True ,"value_types" TEXT = 'None' ,"allowed_types" TEXT = '' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].pyset(nb_elements
,variable_nb_elements
,value_types
,allowed_types
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION pylist("nb_elements" INTEGER = 10 ,"variable_nb_elements" BOOLEAN = True ,"value_types" TEXT = 'None' ,"allowed_types" TEXT = '' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].pylist(nb_elements
,variable_nb_elements
,value_types
,allowed_types
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION pyiterable("nb_elements" INTEGER = 10 ,"variable_nb_elements" BOOLEAN = True ,"value_types" TEXT = 'None' ,"allowed_types" TEXT = '' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].pyiterable(nb_elements
,variable_nb_elements
,value_types
,allowed_types
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION pydict("nb_elements" INTEGER = 10 ,"variable_nb_elements" BOOLEAN = True ,"value_types" TEXT = 'None' ,"allowed_types" TEXT = '' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].pydict(nb_elements
,variable_nb_elements
,value_types
,allowed_types
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION pystruct("count" INTEGER = 10 ,"value_types" TEXT = 'None' ,"allowed_types" TEXT = '' ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].pystruct(count
,value_types
,allowed_types
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : ssn
-- https://github.com/joke2k/faker/blob/master/faker/providers/ssn/__init__.py


CREATE OR REPLACE FUNCTION ssn() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].ssn()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



-- Provider : user_agent
-- https://github.com/joke2k/faker/blob/master/faker/providers/user_agent/__init__.py


CREATE OR REPLACE FUNCTION mac_processor() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].mac_processor()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION linux_processor() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].linux_processor()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION user_agent() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].user_agent()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION chrome("version_from" INTEGER = 13 ,"version_to" INTEGER = 63 ,"build_from" INTEGER = 800 ,"build_to" INTEGER = 899 ) RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].chrome(version_from
,version_to
,build_from
,build_to
)
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION firefox() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].firefox()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION safari() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].safari()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION opera() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].opera()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION internet_explorer() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].internet_explorer()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION windows_platform_token() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].windows_platform_token()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION linux_platform_token() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].linux_platform_token()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION mac_platform_token() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].mac_platform_token()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION android_platform_token() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].android_platform_token()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;

CREATE OR REPLACE FUNCTION ios_platform_token() RETURNS TEXT
AS $$
try:
  GD['Faker']
except KeyError:
  plpy.error(
    "faker is not initialized.",
    hint="Use SELECT faker.faker(); first."
  )
return GD['Faker'].ios_platform_token()
$$
  LANGUAGE plpython3u
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY INVOKER
  PARALLEL SAFE
;



