CHANGELOG
===============================================================================

20220310: 0.5.3 - Fixup Install
-------------------------------------------------------------------------------

__Dependencies:__

* faker 6.1.1

__Changes:__

* FIX #36: control file is duplicated

20220309: 0.5.2 - Fixup RPM package
-------------------------------------------------------------------------------

__Dependencies:__

* faker 6.1.1

__Changes:__

* FIX #34: add missing file

20211216: 0.5.1 - Fixup PGXN package
-------------------------------------------------------------------------------

__Dependencies:__

* faker 6.1.1

__Changes:__

* [pgxn] include the new .c file


20211216: 0.5.0 - Auto init
-------------------------------------------------------------------------------

__Dependencies:__

* faker 6.1.1

__Changes:__

* [Faker] Auto Initialize the engine
* [doc] Link to other Faker engines for PostgreSQL
* [CI] Switch to buster
* [Faker] FIX #24: Replace expected errors by warnings
* Support Postgres 14


20210509: 0.4.0 - More functions !
-------------------------------------------------------------------------------

__Dependencies:__

* faker 6.1.1

__Changes:__

* [Faker] Add a `_functions()` function
* [Faker] Add all the localized functions (@gianluca.toso)
* [Faker] Add unique functions (@gianluca.toso)


20210423: 0.3.0 - Better CI and fixed install process
-------------------------------------------------------------------------------

__Dependencies:__

* faker 8.1.0

__Changes:__

* [CI] add SAST
* [CI] disable tests for PG 9 and PG 10
* [Faker] Freeze version of Faker / Switch to version 8.1.0
* [doc] FIX #14 README fixes (@devrimgunduz)
* [install] FIX #13 tarball does not compile (@devrimgunduz)
* [install] FIX #8 fails to install on PGXN (@sdomi)


20210421: 0.2.0 - minor fixes
-------------------------------------------------------------------------------

__Dependencies:__

* faker 4.1.1

__Changes:__

* Fix #2: PGXN packaging
* [CI] Simplify
* FIX #5: Handle correctly `None` default values


20200814: 0.1.0 - initial version
-------------------------------------------------------------------------------

__Dependencies:__

* faker 4.1.1

__Changes:__

* Proof of Concept
