Fake Data Generator for PostgreSQL
===============================================================================

`postgresql_faker` is a PostgreSQL extension based on the awesome
[Python Faker Library]. This is useful to generate random-but-meaningful
datasets for functionnal testing, anonymization, training data, etc...

This extension is simply a wrapper written in [pl/python] procedural language.

[Python Faker Library]: http://faker.rtfd.org/
[pl/python]: https://www.postgresql.org/docs/current/plpython.html

Demo
-------------------------------------------------------------------------------

You create and init the extension like this:

```sql
CREATE SCHEMA faker;

CREATE EXTENSION faker SCHEMA faker CASCADE;

SELECT faker.name();
     name
---------------
 Edith Navarro
(1 row)
```

Then you can populate easily a table like this:

```sql
CREATE TABLE users (
  id BIGINT,
  firstname TEXT,
  lastname TEXT,
  email TEXT,
  birth DATE,
  ssn TEXT
);

INSERT INTO users AS
SELECT
  faker.ean()::BIGINT,
  faker.first_name(),
  faker.last_name(),
  faker.company_email(),
  faker.date_this_century()::DATE - INTERVAL '30 year',
  faker.ssn()
FROM generate_series(1,1000);
```


Providers
-------------------------------------------------------------------------------

The faker extension offers 500 functions such as `name()`, `address()`, etc.
These functions are regrouped in various providers:

* [Standard Providers] are generic and cover most of your needs
* [Localized Providers] produce language-specific data (see [Localization])

The complete list is available with `SELECT faker._functions();`

Please note that [Community Providers] are not supported.

[Standard Providers]: https://faker.readthedocs.io/en/master/providers.html
[Community Providers]: https://faker.readthedocs.io/en/master/communityproviders.html
[Localized Providers]: https://faker.readthedocs.io/en/master/locales.html
[Localization]: #Localization

Install
-------------------------------------------------------------------------------

You need to install python3, pl/python and the Faker library.

### RedHat / Rocky

Add the [PostgreSQL Official RPM Repo] to your system.

```bash
sudo yum install https://.../pgdg-redhat-repo-latest.noarch.rpm
sudo yum module disable postgresql
sudo yum install postgresql_faker_13
```

Replace `13` with the major version of your PostgreSQL instance.

[PostgreSQL Official RPM Repo]: https://yum.postgresql.org/


### Debian / ubuntu

```bash
apt install python3-pip postgresql-plpython3-13 pgxnclient
pip install faker
pgxn install postgresql_faker
```

Replace `13` with the major version of your PostgreSQL instance.

### From source

```bash
make extension
sudo make install
```

Unique Values
-------------------------------------------------------------------------------

With the `unique_` prefix before any faking function, you can guarantee that
any generated values are unique **within the current session**. Uniqueness is
not guaranteed across multiples sessions.

For instance, in order to get a unique name, you can use `unique_name()`
instead of `name()`. The `unique_` functions have the same parameters that
the regular ones.

```sql
SELECT faker.faker('fr_FR');

SELECT COUNT( DISTINCT faker.department())
FROM generate_series(1,100);
 count
-------
    65

SELECT COUNT( DISTINCT faker.unique_department())
FROM generate_series(1,100);
 count
-------
   100
```

Calling `SELECT faker_unique.clear()` clears the already seen values. To avoid
infinite loops, after a number of attempts to find a unique value, Faker will
throw a UniquenessException.

```sql
SELECT faker.unique_null_boolean();
FROM generate_series(1,3);

SELECT faker.unique_null_boolean();
ERROR:  faker.exceptions.UniquenessException. Got duplicated values after 1,000 iterations.

SELECT faker.unique_clear();
 unique_clear
--------------
 t
(1 row)

SELECT faker.unique_null_boolean();
 unique_null_boolean
---------------------
 False
(1 row)
```

> _IMPORTANT:_ The uniqueness cache is not shared between sessions


Localization
-------------------------------------------------------------------------------

`faker.faker()` can take a locale as an argument, to return localized data.
If no localized provider is specified, the fallback is the `en_US` locale.

```sql
SELECT faker.faker('it_IT');
SELECT faker.name();
     name
---------------------------
   Sig. Alighieri Monti
```

`faker.faker()` also supports multiple locales.

```sql
SELECT faker.faker(ARRAY['it_IT', 'en_US', 'ja_JP']);
```

The `faker.faker()` initialization needs to be done at the start of each new
session. If you want to define the locales permanently, just define the
parameter `faker.locales` in `postgresql.conf`

```ini
faker.locales = 'it_IT, en_US, ja_JP'
```

You can also define it at the database level :

```sql
ALTER DATABASE mydb SET faker.locales = 'it_IT, en_US, ja_JP';
```

For the complete list of available locale, please read the Faker doc:

https://faker.readthedocs.io/en/master/locales.html

Seeding The Generator
-------------------------------------------------------------------------------

When using Faker for unit testing, you will often want to generate the same
data set. For convenience, the extension provides a `seed()` function,
which seeds the shared random number generator. Calling the same methods
with the same version of faker and seed produces the same results.

```sql
SELECT faker.faker();

SELECT faker.seed(4321);
SELECT faker.name() FROM generate_series(1,3);
    name
-------------
 Jason Brown
 Jacob Stein
 Cody Brown
(3 rows)

SELECT faker.seed(4321);
SELECT faker.name() FROM generate_series(1,3);
    name
-------------
 Jason Brown
 Jacob Stein
 Cody Brown
(3 rows)
```

If you want to define the seed permanently, just definte the parameter
`faker.seed` in `postgresql.conf`

```ini
faker.seed = '4321'
```

You can also define it at the database level :

```sql
ALTER DATABASE mydb SET faker.seed = '4321';
```


Limitations
-------------------------------------------------------------------------------

### Casting

All function will return `TEXT` values. You need to convert the result into the
data type you want:

```sql
# SELECT faker.year() = 2020;
ERROR:  operator does not exist: text = integer

# SELECT faker.year()::INT = 2020;
 ?column?
----------
 f
(1 row)
```

### Reserved keywords

The python Faker methods `time()` and `date()` are not available because their
name are reserved PostgreSQL keywords.

* instead of `faker.date()`, you can use `faker.date_between()`
* instead of `faker.time()`, you can use `(random() * interval '1d')::TIME`


Other Faker engines for PostgreSQL
-------------------------------------------------------------------------------

* [faker_fdw](https://github.com/guedes/faker_fdw)
  A foreign data wrapper based on python Faker

* [pgsynthdata](https://gitlab.com/geometalab/pgsynthdata)
  A Python tool that generates synthetic data into a PostgreSQL database


Licence
-------------------------------------------------------------------------------

`postgresql_faker` is released under the PostgreSQL License.


