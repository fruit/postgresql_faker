--
-- Address provider
-- https://github.com/joke2k/faker/blob/master/faker/providers/address/__init__.py
--

CREATE OR REPLACE FUNCTION address() RETURNS TEXT
AS $$ return GD['exec'](GD['sys']._getframe()) $$ LANGUAGE plpython3u;

CREATE OR REPLACE FUNCTION country()  RETURNS TEXT
AS $$ return GD['exec'](GD['sys']._getframe()) $$ LANGUAGE plpython3u;
