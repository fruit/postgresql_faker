/*
 * faker.c
 *
 */
#include "postgres.h"
#include "utils/guc.h"

PG_MODULE_MAGIC;

/* Entrypoint of the module */
void  _PG_init(void);

PG_FUNCTION_INFO_V1(faker_dummy);

static bool guc_faker_autoinit;
static char *guc_faker_locales;
static char *guc_faker_seed;

void
_PG_init(void)
{

  /* GUC parameters */
  DefineCustomBoolVariable
  (
    "faker.autoinit",
    "Initialize the faker engine when necessary",
    "Disable this if you want to manually initialize the engine in each session",
    &guc_faker_autoinit,
    true,
    PGC_SUSET,
    0,
    NULL,
    NULL,
    NULL
  );

  DefineCustomStringVariable
  (
    "faker.locales",
    "List of locales which will be used to ",
    "Read Faker project documentation for the complete list of available locales",
    &guc_faker_locales,
    "",
    PGC_SUSET,
    GUC_LIST_INPUT,
    NULL,
    NULL,
    NULL
  );


  DefineCustomStringVariable
  (
    "faker.seed",
    "Initialize the random generator",
    "The faker engine will produce the same fake values for each new session",
    &guc_faker_seed,
    "",
    PGC_SUSET,
    0,
    NULL,
    NULL,
    NULL
  );

}

/*
 * This function is here just so that the extension is not completely empty
 * and the dynamic library is loaded when CREATE EXTENSION runs.
 */
Datum
faker_dummy(PG_FUNCTION_ARGS)
{
	PG_RETURN_VOID();
}
